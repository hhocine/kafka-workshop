#### This exercise aims at interacting with the schema registry API

#### Start the Kafka cluster :
In the CONSOLE directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry 
The docker-compose file contains also a base container that allows to run Kafka commands

#### Schema registry API
Using a API testing tool (Postman),
- get the global compatibility of the schema registry  
Compatibility is checked against all existing schemas   
Fields can be added, and optional field (with a default value) can be deleted  
```
GET http://localhost:8081/config/
```     
- update the global compatibility to FORWARD_TRANSITIVE  
```
PUT http://localhost:8081/config/
{
  "compatibility": "FORWARD_TRANSITIVE"
} 	
```
- post a schema named SCHEMA-TEST
```
POST  http://localhost:8081/subjects/SCHEMA-TEST/versions  
{
    "schema": "{\"type\":\"record\",\"name\":\"schemaTest\",\"namespace\":\"com.kafka.workshop.avro\",\"doc\":\"this is a schema test\",\"fields\":[{\"name\":\"keyField1\",\"type\":\"string\",\"default\":\"keyField1\"},{\"name\":\"keyField2\",\"type\":\"string\",\"default\":\"keyField2\"}]}"
} 
```
- post a modification of SCHEMA-TEST
```
POST  http://localhost:8081/subjects/SCHEMA-TEST/versions  
{
    "schema": "{\"type\":\"record\",\"name\":\"schemaTest\",\"namespace\":\"com.kafka.workshop.avro\",\"doc\":\"this is a schema test\",\"fields\":[{\"name\":\"keyField1\",\"type\":\"string\",\"default\":\"keyField1\"},{\"name\":\"keyField2\",\"type\":\"string\",\"default\":\"keyField2\"},{\"name\":\"keyField3\",\"type\":\"string\",\"default\":\"keyField3\"}]}"
} 
```
- get the list of schemas
```
GET http://localhost:8082/subjects/
```
- get the list of versions of SCHEMA-TEST
```
GET http://localhost:8082/subjects/SCHEMA-TEST/versions
```
- get the detail of the version 2 of SCHEMA-TEST
```
GET http://localhost:8082/subjects/SCHEMA-TEST/versions/2
 ```
- get a pretty detail of the version 2 of SCHEMA-TEST
```
GET http://localhost:8082/subjects/SCHEMA-TEST/versions/2/schema
```
- remove the two versions of SCHEMA-TEST
```
DELETE  http://localhost:8081/subjects/SCHEMA-TEST/versions/1  
DELETE  http://localhost:8081/subjects/SCHEMA-TEST/versions/2
```