package solution.com.kafka.workshop.consumer;


import com.kafka.workshop.avro.WorkshopKey;
import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SaslConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@Component
public class AvroConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(AvroConsumer.class);

    // TODO: Set the topic name
    private String topic = "avroTopic";


    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:19092,localhost:29092,localhost:39092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid1");
        // TODO: Set the Schema Registry url
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // TODO: Set the key and value serializers KafkaAvroSerializer
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        // TODO: Set KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);

        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);


        boolean isGeneric = false;
        if(isGeneric) {
            Consumer<WorkshopKey, WorkshopValue> consumer = new KafkaConsumer<>(props);
            // TODO: Subscribe to topic
            consumer.subscribe(Arrays.asList(topic));


            try {
                while (true) {

                    // TODO: Poll records and iterate on each records
                    // TODO: Display the record key, the record value, the record partition and the record offset
                    ConsumerRecords<WorkshopKey, WorkshopValue> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                    consumerRecords.forEach(record -> {

                        WorkshopKey workshopKey = record.key();
                        WorkshopValue workshopValue = record.value();
                        logger.info("consuming message key={} value={}", workshopKey, workshopValue);


                    });
                    //commiting to cluster, kafka.consumer.autocommit is false
                    consumer.commitSync();
                }
            } finally {
                consumer.close();
            }
        }else {
            Consumer<GenericRecord, GenericRecord> consumerGeneric = new KafkaConsumer<>(props);
            consumerGeneric.subscribe(Arrays.asList(topic));
            try {
                while (true) {

                    // TODO: Poll records and iterate on each records
                    // TODO: Display the record key, the record value, the record partition and the record offset
                    ConsumerRecords<GenericRecord, GenericRecord> consumerRecords = consumerGeneric.poll(Duration.ofMillis(1000));
                    consumerRecords.forEach(record -> {

                        GenericRecord workshopKey = record.key();
                        GenericRecord workshopValue = record.value();
                        logger.info("consuming message key={} value={}", workshopKey, workshopValue);


                    });
                    //commiting to cluster, kafka.consumer.autocommit is false
                    consumerGeneric.commitSync();
                }
            } finally {
                consumerGeneric.close();
            }
        }

    }

}
