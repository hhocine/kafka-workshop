package partial.com.kafka.workshop.consumer;

import com.kafka.workshop.avro.WorkshopKey;
import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SaslConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class AvroConsumer implements ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = "";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:19092,localhost:29092,localhost:39092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid");

        // TODO: Set the Schema Registry url

        // TODO: Set the key and value serializers KafkaAvroSerializer

        // TODO: Set KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG


        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        boolean isGeneric = false;
        if(isGeneric) {
            Consumer<WorkshopKey, WorkshopValue> consumer = new KafkaConsumer<>(props);

            // TODO: Subscribe to topic

            try {
                while (true) {

                    // TODO: Poll records and iterate on each records
                    // TODO: Display the record key, the record value, the record partition and the record offset

                    consumer.commitSync();
                }
            } finally {
                consumer.close();
            }
        }else{
            Consumer<GenericRecord, GenericRecord> consumerGeneric = new KafkaConsumer<>(props);
            // TODO: Subscribe to topic

            try {
                while (true) {

                    // TODO: Poll records and iterate on each records
                    // TODO: Display the record key, the record value, the record partition and the record offset

                    consumerGeneric.commitSync();
                }
            } finally {
                consumerGeneric.close();
            }
        }


    }



}
