package partial.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class AvroConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(AvroConsumerApplication.class);
        application.run(args);
    }
	
}
