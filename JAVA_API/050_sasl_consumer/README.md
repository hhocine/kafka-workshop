#### This exercise aims at writing a java KAFKA consumer using SASL authentication

#### Start the Kafka cluster :
If your Docker containers of the previous the exercise 05_sasl_producer are stopped, start them  
```docker-compose start```

The cluster is composed of 1 zookeeper and 3 brokers  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  
The cluster is configured to use SASL_PLAINTEXT protocol with the SCRAM-SHA-512 SASL mechanism  
```advertised.listeners=SASL_PLAINTEXT://localhost:19092```  
```listeners=SASL_PLAINTEXT://0.0.0.0:29092```  
```security.inter.broker.protocol=SASL_PLAINTEXT```  
```sasl.enabled.mechanisms=SCRAM-SHA-512```  
```sasl.mechanism.inter.broker.protocol=SCRAM-SHA-512```  
Each broker is started with KAFKA_OPTS options:```-Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf```  
Each zookeeper is started with KAFKA_OPTS options: ```-Djava.security.auth.login.config=/etc/kafka/secrets/zookeeper_jaas.conf```  

##### In the partial.com.kafka.workshop.producer.SaslConsumer java class:
#### Topic name :
Set the topic name you will produce records to using "topic" variable. Set it to saslTopic  
#### Configure the SASL authentication with an admin user
Configure the KAFKA consumer using the org.apache.kafka.common.config.SaslConfigs and org.apache.kafka.clients.CommonClientConfigs classes  
- the security protocol configuration using the CommonClientConfigs.SECURITY_PROTOCOL_CONFIG constant. Set it to SASL_PLAINTEXT
- the SASL mechanism configuration using the SaslConfigs.SASL_MECHANISM constant. Set it to SCRAM-SHA-512
- the SASL JAAS configuration using the SaslConfigs.SASL_JAAS_CONFIG constants. It must contains credentials created previously:
```"org.apache.kafka.common.security.scram.ScramLoginModule required username=\"myUser\" password=\"myAdminUserPassword\";");```  
Start the consumer  
Records are consumed successfully   	
#### Configure the SASL authentication with a non admin user
Change the SASL JAAS configuration in order to authenticate the cluster with myUser  
Start the consumer  
Note the error ```"Caused by: org.apache.kafka.common.errors.GroupAuthorizationException: Not authorized to access group: groupid_user"```  
ACLs must be added to the myUser to READ records to saslTopic  
The consumer group can also be specified, set it to '*' to apply to all groups  
Get into the base container  
```docker exec -it base bash```  
Using the kafka-acl command, add the READ to saslTopic authorization to the myUser user  
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --add --allow-principal User:myUser --operation READ --topic saslTopic --group '*'```  
List the existing ACLs  
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --list```  
Exit the container  
```exit;```  
Change the ConsumerConfig.GROUP_ID_CONFIG and start the consumer  
Records are successfully consumed            
