package solution.com.kafka.workshop.consumer;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
@Component
public class SaslConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = "saslTopic";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:19092,localhost:29092,localhost:39092");
       // props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid_user");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 100000);
        // TODO: Add SASL configuration
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_PLAINTEXT");
        props.put(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512");
   //     props.put(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"admin\" password=\"admin-secret\";");
        props.put(SaslConfigs.SASL_JAAS_CONFIG, "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"myUser\" password=\"myUserPassword\";");

        Consumer<String, String> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(Arrays.asList(topic));

        try {
            while (true) {
                ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                consumerRecords.forEach(record -> {
                    if(record.headers() == null || record.headers().lastHeader("record_key") == null){
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset());
                    }else{
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset(), new String(record.headers().lastHeader("record_key").value()));
                    }
                });
                consumer.commitSync();
            }
        }finally {
            consumer.close();
        }

    }



}
