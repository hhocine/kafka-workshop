package partial.com.kafka.workshop.consumer;


import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
@Component
public class SimpleConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = "";

    private boolean seekTobeginning = true;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        // TODO: Set the bootstrap servers

        // TODO: Set the group id

        // TODO: Set the key and value deserializers

        // TODO: Set the auto reset offset to the earliest commited offset

        // TODO: Set the auto commit config to false

        // TODO: Create an instance of a Consumer with String key and Value key
        Consumer<String, String> consumer = null;

        // TODO: Subscribe to topic


        // TODO: Seek to beginning. While partition assignment is not done, poll(0)
        if(seekTobeginning) {

        }

        try {
            while (true) {
                // TODO: Poll records and iterate on each records
                ConsumerRecords<String, String> consumerRecords = null;
                consumerRecords.forEach(record -> {

                });
                // TODO: Display the record key, the record value, the record partition and the record offset
                // TODO: Display the record header. Tips: get the last header value

                // TODO Commit last offset polled
             }
        }finally {
            consumer.close();
        }

    }
}

