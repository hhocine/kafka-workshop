package solution.com.kafka.workshop.consumer;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
@Component
public class SimpleConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = "simpleTopic";

    private boolean seekTobeginning = false;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        // TODO: Set the bootstrap servers
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:19092,localhost:29092,localhost:39092");
        // TODO: Set the group id
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid");

        // TODO: Set the key and value deserializers
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        // TODO: Set the auto reset offset to the earliest commited offset
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO: Set the auto commit config to false
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        // TODO: Create an instance of a Consumer with String key and Value key
        Consumer<String, String> consumer = new KafkaConsumer<>(props);

        // TODO: Subscribe to topic
        consumer.subscribe(Arrays.asList(topic));

        // TODO: Seek to beginning. While partition assignment is not done, poll(0)
        if(seekTobeginning) {
            while (consumer.assignment().size() == 0) {
                consumer.poll(Duration.ofMillis(0));
            }
            consumer.seekToBeginning(consumer.assignment());
        }

        try {
            while (true) {
                // TODO: Poll records and iterate on each records
                // TODO: Display the record key, the record value, the record partition and the record offset
                // TODO: Display the record header. Tips: get the last header value
                ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                consumerRecords.forEach(record -> {
                    if(record.headers() == null || record.headers().lastHeader("record_key") == null){
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset());
                    }else{
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset(), new String(record.headers().lastHeader("record_key").value()));
                    }

                });

                // TODO Commit last offset polled
                consumer.commitSync();
            }
        }finally {
            consumer.close();
        }

    }



}
