package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class SpringbootConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SpringbootConsumerApplication.class);
        application.run(args);
    }
	
}
