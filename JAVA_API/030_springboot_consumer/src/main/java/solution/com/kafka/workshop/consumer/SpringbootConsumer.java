package solution.com.kafka.workshop.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SpringbootConsumer implements ConsumerSeekAware {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    private boolean seekFromBeginning = false;

    //TODO add the @KafakaListener annotation
    @KafkaListener(topics = "simpleTopic", containerFactory = "kafkaListenerContainerFactory")
    public void listenToTopic(ConsumerRecord<String, String> record ) {
        if(record.headers() == null || record.headers().lastHeader("record_key") == null){
            logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset());
        }else{
            logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset(), new String(record.headers().lastHeader("record_key").value()));
        }
    }

    @Override
    public void registerSeekCallback(ConsumerSeekCallback callback) {

    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        if(seekFromBeginning) {
            assignments.forEach(
                    (p, offset) -> {
                        logger.info("seeking to beginning for partion {}", p.partition());
                        callback.seekToBeginning(p.topic(), p.partition());
                    }
            );
        }
    }

    @Override
    public void onIdleContainer(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {

    }
}
