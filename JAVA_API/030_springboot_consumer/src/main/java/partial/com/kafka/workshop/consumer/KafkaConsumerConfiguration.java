package partial.com.kafka.workshop.consumer;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.Properties;

@Configuration
@EnableKafka

public class KafkaConsumerConfiguration {

    @Autowired
    private ConsumerFactory<String, String> consumerFactory;

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {

        Properties props = new Properties();
        // TODO: Set the bootstrap servers

        // TODO: Set the group id

        // TODO: Set the key and value deserializers

        // TODO: Set the auto reset offset to the earliest commited offset

        // TODO: Set the auto commit config to false

        return new DefaultKafkaConsumerFactory(props);
    }

    @Bean
    ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);

        return factory;
    }





}
