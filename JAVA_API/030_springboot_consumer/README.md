### This exercise aims at writing a simple java KAFKA consumer using SpringBoot implementation

#### Start the Kafka cluster :
If your Docker containers of the previous the exercise 02_simple_producer are stopped, start them
```docker-compose start```

#### In the partial.com.kafka.workshop.consumer.SpringbootConsumer java class:
#### Topic name:
Set the topic name you will consume records from using "topic" variable
#### Consumer configuration :
Configure the KAFKA consumer in the KafkaConsumerConfiguration class using the org.apache.kafka.clients.consumer.ConsumerConfig class  
- the broker list user the BOOTSTRAP_SERVERS_CONFIG constant
- the consumer group id using the GROUP_ID_CONFIG constant
- the deserializer for the key and the value of the record to consume using the KEY_DESERIALIZER_CLASS_CONFIG and VALUE_DESERIALIZER_CLASS_CONFIG constants  
We will consume some String messages: StringDeserializer.class
- from wich offset to consumer when there is no initial offset or if the current offset does not exist any more using the AUTO_OFFSET_RESET_CONFIG constant
earliest: automatically reset the offset to the earliest offset  
latest: automatically reset the offset to the latest offset  
none: throw exception to the consumer if no previous offset is found for the consumer's group  
- the autocommit strategy using the ENABLE_AUTO_COMMIT_CONFIG constant (set it to false)
#### Consume records :
In the SpringbootConsumer class
- create a public method annotated with the @KafakaListener annotation, set the topic name and the container factory defined in the KafkaConsumerConfiguration class
-  display the key, value, offset, partition, topic and the header of each record using the Logger  
#### Consume records from beginning:
- modify the SpringbootConsumer class and make it implement org.springframework.kafka.listener.ConsumerSeekAware
- modify the onPartitionsAssigned method to seek form beginning
use assignments.forEach((p,offset) -> {...}); method

