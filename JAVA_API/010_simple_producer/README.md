#### This exercise aims at writing a simple java KAFKA producer

#### Start the Kafka cluster :
Start the Kafka Cluster using the docker-compose file  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  


##### In the partial.com.kafka.workshop.producer.SimpleProducer java class:
#### Topic name:
Set the topic name you will produce records to using in the "topic" variable. Set it to *simpleTopic*
#### Producer configuration :
Configure the KAFKA producer using the org.apache.kafka.clients.producer.ProducerConfig class
- the broker list using the BOOTSTRAP_SERVERS_CONFIG constant. A list of your host/port in the form host1:port1,host2:port2,...
  The host is your docker-machine IPAddress (for ex: *"localhost:19092,localhost:29092,localhost:39092"*)
- the acks configuration using the ACKS_CONFIG constant. Set it to ALL
- the retries configuration using the RETRIES_CONFIG constant. Set to 10
- the batch size config to default value 16384
- set the linger ms config to default value 0
- the serializer for the key and the value of the record to produce using the KEY_SERIALIZER_CLASS_CONFIG and VALUE_SERIALIZER_CLASS_CONFIG constants
  in this exercise, String records will be produced: StringSerializer.class
#### Producing records :
- Create a org.apache.kafka.clients.producer.KafkaProducer instance
- Inside the For Loop, create a org.apache.kafka.clients.producer.ProducerRecord instance, pass the topic name, the key and value as arguments
- Send each record to Kafka topic using the send(Record) method of the Producer instance					
- Send each record to Kafka topic using the producer.send(Record, new Callback() {...} ).get() method
  Log the topic, partition and offset of the record if no error is thrown
#### Adding headers to records :
In the buildHeader method of the SimpleProducer class:
- Create a org.apache.kafka.common.header.internals.RecordHeader instance. A string key and byte array as value must be passed as constructor arguments  
  For ex, for each record add the ("record_key", key) as header
- Add the RecordHeader to the Headers of the record using the headers().add(recordHeader) method of a record 

#### Produce with stopped brokers
Get the list of running container ids  
```docker ps```
Stop one running broker container  
```docker stop 'id'```
Produce some records, note that production is made successfully  
Stop one more running broker  
```docker stop 'id'```  
Produce some records, because the isr is not fulfilled production is not possible anymore  
```  KAFKA ERROR topic=simpleTopic partition=0 offset=-1 error=Messages are rejected since there are fewer in-sync replicas than required. ```  
Change the ack configuration to 1  
Produce some records, note that production is made successfully  
Change the ack configuration to all  
Restart all brokers  
```docker-compose up -d```  

#### Tune the record production
Alter the batch size configuration to max value 33554432
Alter the linger ms config to 1000 ms
Produce records and observe how production takes longer

#### Consume records from Java application
Go to the next exercice: 020_simple_consumer  
Do not stop your containers !


