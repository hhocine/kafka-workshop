package partial.com.kafka.workshop.producer;

import com.kafka.workshop.avro.*;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AvroProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(AvroProducer.class);

    // TODO: Set the topic name
    private String topic = "";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:19092,localhost:29092,localhost:39092");

        // TODO: Set the Schema Registry url

        // TODO: Set the key and value serializers KafkaAvroSerializer

        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 3);

        // TODO: Create an Producer instance with WorkshopKey key and WorkshopValue key
        Producer producer = null;

        try {

            for(int i=0; i<10; i++) {

                WorkshopKey workshopKey = WorkshopKey.newBuilder()
                        .setKeyField1(getAlphaNumericString(3))
                        .setKeyField2(getAlphaNumericString(3))
                        .build();

                WorkshopN workshopN = WorkshopN.newBuilder()
                        .setNestedField(getAlphaNumericString(5))
                     //   .setNestedField2(getAlphaNumericString(5))
                        .build();
                WorkshopL workshopL = WorkshopL.newBuilder()
                        .setListWorkshopField(getAlphaNumericString(10))
                        .build();
                List<WorkshopL> list = new ArrayList<>();
                list.add(workshopL);

                WorkshopM workshopM = WorkshopM.newBuilder()
                        .setMapWorkshopField(getAlphaNumericString(10))
                        .build();
                Map<String, WorkshopM> map = new HashMap<>();
                map.put(getAlphaNumericString(3), workshopM);

                WorkshopValue workshopValue = WorkshopValue.newBuilder()
                        .setValueField1(getAlphaNumericString(4))
                        .setValueField2(getInteger())
                        .setValueField3(getAlphaNumericString(5))
                      //  .setValueField4(getAlphaNumericString(5))
                        .setListWorkshop(list)
                        .setMapWorkshop(map)
                        .setNestedWorkshop(workshopN)
                        .build();

                // TODO: Send record

            }

        } finally {
            producer.close();
        }

    }

    private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private Integer getInteger() {
        int number = (int) (Math.random() * 1000);
        return number;
    }





}
