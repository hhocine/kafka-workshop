#### This exercise aims at writing a KSTREAM java application consuming String records


The objective of this exercise is to :  
- read records from merge-stat-topic topic as a KStream  
The topic contains String key / String value pair  
- display the stream content  

#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Configure the KSTREAM topology
Configure the stream topology following the TODO instructions in the solution.com.kafka.workshop.SimpleKStream java class

####Start the Stream application
Start the application  
Check that messages are consumed correctly and continuously    

#### Produce example records
Use the com.kafka.workshop.init.producer.SimpleStreamProducerApplication class to produce records to simple-topic  
The produce records are String / String key values    


        