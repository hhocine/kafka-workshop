package com.kafka.workshop.init.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class SimpleStreamProducerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SimpleStreamProducerApplication.class);
        application.run(args);
    }

}
