package solution.com.kafka.workshop;

import com.michelin.workshop.avro.WorkshopRef;
import com.michelin.workshop.avro.WorkshopConsolidated;
import com.michelin.workshop.avro.WorkshopStats;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TestingStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(TestingStream.class);

    private String refTopic;
    private String statsTopic;
    private String rekeyedRefTopic;
    private String consolidatedTopic;
    private String alertTopic;
    private String bootstrapServers;
    private String schemaRegistry;
    private String applicationId;
    private String clientId;
    private String autoOffset;

    private Serde<WorkshopRef> refAvroSerde = new SpecificAvroSerde<>();
    private Serde<WorkshopRef> rekeyedRefAvroSerde = new SpecificAvroSerde<>();
    private Serde<WorkshopStats> statsAvroSerde = new SpecificAvroSerde<>();

    private Serde<WorkshopConsolidated> consolidatedDatasAvroSerde = new SpecificAvroSerde<>();

    @Autowired
    public TestingStream(
            @Value("${topic.refTopic}")
                    String refTopic,
            @Value("${topic.statsTopic}")
                    String statsTopic,
            @Value("${topic.rekeyedRefTopic}")
                    String rekeyedRefTopic,
            @Value("${topic.consolidatedTopic}")
                    String consolidatedTopic,
            @Value("${topic.alertTopic}")
                    String alertTopic,
            @Value("${bootstrap-servers}")
                    String bootstrapServers,
            @Value("${schemaRegistry-urls}")
                    String schemaRegistry,
            @Value("${application.id}")
                    String applicationId,
            @Value("${client.id}")
                    String clientId,
            @Value("${auto.offset}")
                    String autoOffset
    ) {
        this.refTopic = refTopic;
        this.statsTopic = statsTopic;
        this.rekeyedRefTopic = rekeyedRefTopic;
        this.consolidatedTopic = consolidatedTopic;
        this.alertTopic = alertTopic;
        this.bootstrapServers = bootstrapServers;
        this.schemaRegistry = schemaRegistry;
        this.applicationId = applicationId;
        this.clientId = clientId;
        this.autoOffset = autoOffset;

        //Setup schemaRegistry serdes
        final Map<String, String> serdeConfig  = Collections.singletonMap(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, this.schemaRegistry);
        this.statsAvroSerde.configure(serdeConfig, false);
        this.refAvroSerde.configure(serdeConfig, false);
        this.rekeyedRefAvroSerde.configure(serdeConfig, false);

        this.consolidatedDatasAvroSerde.configure(serdeConfig, false);


    }

    @Override
    public void run(ApplicationArguments args) {

      try {
            createTopics();
            // Create KafkaStreams instance with properties, TIPS: use createStream method
            KafkaStreams streams = createStream(configureStream());

            // Start the stream
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    public KafkaStreams createStream(Properties streamsConfiguration) {
        //  Create a topology instance
        Topology topology = buildTopology();
        // Create a KafkaStreams instance and return it
        KafkaStreams kafkaStreams = new KafkaStreams(topology, streamsConfiguration);
        return kafkaStreams;
    }

    public Topology buildTopology() {
        // Create StreamsBuilder instance
        final StreamsBuilder builder = new StreamsBuilder();

        // build a KTable from tireTopic after re keying
        KStream<String, WorkshopRef>tireStream = builder.stream(refTopic, Consumed.with(Serdes.String(), this.refAvroSerde))
                .map((key, tire) -> new KeyValue<>(tire.getIdent(), tire));
        tireStream.to(rekeyedRefTopic, Produced.with(Serdes.String(), this.rekeyedRefAvroSerde));
        KTable<String, WorkshopRef> tiresTable = builder.table(rekeyedRefTopic, Consumed.with(Serdes.String(), this.rekeyedRefAvroSerde));

        // build the datas stream topology
        KStream<String, WorkshopStats> datasStream = builder.stream(statsTopic, Consumed.with(Serdes.String(), this.statsAvroSerde))
                .map((key, datas) -> new KeyValue<>(datas.getIdent(), datas))
                .filter((key, datas) -> datas.getPressure() > 0 && datas.getTemperature() > 0);

        //merging KTable and KStream
        DatasJoiner datasJoiner = new DatasJoiner();
        KStream<String, WorkshopConsolidated> consolidatedDatasStream =
                datasStream.join(tiresTable, datasJoiner, Joined.valueSerde(statsAvroSerde));

        // produce to datas and alerting
        consolidatedDatasStream.to(consolidatedTopic, Produced.with(Serdes.String(), this.consolidatedDatasAvroSerde));
        KStream<String, WorkshopConsolidated> alertStream =
                consolidatedDatasStream.filter((name, consolidatedDatas) ->
                        consolidatedDatas.getPressure() < 1.5 || consolidatedDatas.getTemperature() > 120);
        alertStream.to(alertTopic, Produced.with(Serdes.String(), this.consolidatedDatasAvroSerde));


        return builder.build();
    }



    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        // Set the Schema Registry url
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistry);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId+UUID.randomUUID());
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, clientId);
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffset);

        return streamsConfiguration;
    }

    public void setRefAvroSerde(Serde<WorkshopRef> refAvroSerde) {
        this.refAvroSerde = refAvroSerde;
    }

    public void setStatsAvroSerde(Serde<WorkshopStats> statsAvroSerde) {
        this.statsAvroSerde = statsAvroSerde;
    }

    public void setConsolidatedDatasAvroSerde(Serde<WorkshopConsolidated> consolidatedDatasAvroSerde) {
        this.consolidatedDatasAvroSerde = consolidatedDatasAvroSerde;
    }

    public void setRekeyedRefAvroSerde(Serde<WorkshopRef> rekeyedRefAvroSerde) {
        this.rekeyedRefAvroSerde = rekeyedRefAvroSerde;
    }

    public void createTopics() {
        Map<String, Object> config = new HashMap<>();
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                refTopic,
                3,
                (short) 1));

        topics.add(new NewTopic(
                statsTopic,
                3,
                (short) 1));


        topics.add(new NewTopic(
                rekeyedRefTopic,
                3,
                (short) 1));

        topics.add(new NewTopic(
                consolidatedTopic,
                3,
                (short) 1));

        topics.add(new NewTopic(
                alertTopic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }
}