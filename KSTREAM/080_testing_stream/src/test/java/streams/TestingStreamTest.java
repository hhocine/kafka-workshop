package streams;

import com.michelin.workshop.avro.WorkshopConsolidated;
import com.michelin.workshop.avro.WorkshopStats;
import com.michelin.workshop.avro.WorkshopRef;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Assert;
import org.junit.Test;
import solution.com.kafka.workshop.DatasJoiner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class TestingStreamTest extends MockedKafkaClusterTestUtils{

    @Test
    public void consolidatedDatasTopic() throws IOException, RestClientException {

        // Fixture
        WorkshopRef workshopRef_1 = WorkshopRef.newBuilder().setIdent("101001").setName("name 101").setDescription("description 101").build();
        WorkshopRef workshopRef_2 = WorkshopRef.newBuilder().setIdent("202002").setName("name 202").setDescription("description 202").build();
        WorkshopRef workshopRef_3 = WorkshopRef.newBuilder().setIdent("303003").setName("name 303").setDescription("description 303").build();

        WorkshopStats workshopStats_1 = WorkshopStats.newBuilder().setIdent("101001").setPressure(3).setTemperature(80).build();
        WorkshopStats workshopStats_2 = WorkshopStats.newBuilder().setIdent("101001").setPressure(0).setTemperature(0).build();
        WorkshopStats workshopStats_3 = WorkshopStats.newBuilder().setIdent("202002").setPressure(1.35).setTemperature(100).build();
        WorkshopStats workshopStats_4 = WorkshopStats.newBuilder().setIdent("202002").setPressure(2).setTemperature(110).build();
        WorkshopStats workshopStats_5 = WorkshopStats.newBuilder().setIdent("202002").setPressure(2).setTemperature(110).build();
        WorkshopStats workshopStats_6 = WorkshopStats.newBuilder().setIdent("303003").setPressure(3.60).setTemperature(140).build();
        // end Fixture

        // Add WorkshopStats to topic
        testDriver.pipeInput(refRecordFactory.create(refTopic, String.valueOf(System.currentTimeMillis()), workshopRef_1));
        testDriver.pipeInput(refRecordFactory.create(refTopic, String.valueOf(System.currentTimeMillis()), workshopRef_2));
        testDriver.pipeInput(refRecordFactory.create(refTopic, String.valueOf(System.currentTimeMillis()), workshopRef_3));

        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_1));
        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_2));
        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_3));
        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_4));
        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_5));
        testDriver.pipeInput(statsRecordFactory.create(statsTopic, String.valueOf(System.currentTimeMillis()), workshopStats_6));

        // Get output records
        List<WorkshopConsolidated> actualOutputDatas = new ArrayList<>();
        while (true) {
            ProducerRecord<String, WorkshopConsolidated>
                    record =
                    testDriver.readOutput(consolidatedTopic, stringDeserializer, consolidatedDatasAvroSerde.deserializer());

            if (record != null) {
                actualOutputDatas.add(record.value());
            } else {
                break;
            }
        }
        // Expected values
        DatasJoiner joiner = new DatasJoiner();
        WorkshopConsolidated consolidatedworkshopStats_1 = joiner.apply(workshopStats_1, workshopRef_1);
        WorkshopConsolidated consolidatedworkshopStats_2 = joiner.apply(workshopStats_3, workshopRef_2);
        WorkshopConsolidated consolidatedworkshopStats_3 = joiner.apply(workshopStats_4, workshopRef_2);
        WorkshopConsolidated consolidatedworkshopStats_4 = joiner.apply(workshopStats_5, workshopRef_2);
        WorkshopConsolidated consolidatedworkshopStats_5 = joiner.apply(workshopStats_6, workshopRef_3);
        final List<WorkshopConsolidated> expectedOutputDatas = asList(consolidatedworkshopStats_1, consolidatedworkshopStats_2, consolidatedworkshopStats_3, consolidatedworkshopStats_4, consolidatedworkshopStats_5);

        Assert.assertEquals(expectedOutputDatas, actualOutputDatas);


        List<WorkshopConsolidated> actualOutputAlert = new ArrayList<>();
        final List<WorkshopConsolidated> expectedOutputAlert = asList(consolidatedworkshopStats_2, consolidatedworkshopStats_5);
        while (true) {
            ProducerRecord<String, WorkshopConsolidated>
                    record =
                    testDriver.readOutput(alertTopic, stringDeserializer, consolidatedDatasAvroSerde.deserializer());

            if (record != null) {
                actualOutputAlert.add(record.value());
            } else {
                break;
            }
        }
        Assert.assertEquals(expectedOutputAlert, actualOutputAlert);

    }



}


