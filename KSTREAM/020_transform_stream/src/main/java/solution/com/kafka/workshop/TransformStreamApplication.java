package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class TransformStreamApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TransformStreamApplication.class);
        application.run(args);
    }
	
}
