package com.workshop.kafka.init.producer;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StreamProducer  implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private static final String topic_in = "transform-topic-in";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        Producer<String, String> producer = new KafkaProducer<>(props);

        try {


            String key;
            String value;

            for(int i=0; i<100; i++) {

                key = getAlphaNumericString(3);
                value = i + "::" +
                        String.format("long description %s",getAlphaNumericString(10))  + "::" +
                        String.format("short description %s",getAlphaNumericString(3))  + "::" +
                        getAlphaNumericString(5);


                // TODO: Send record
                ProducerRecord<String, String> record = new ProducerRecord<>(topic_in, key, value);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }


}
