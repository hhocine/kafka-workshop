package com.workshop.kafka.init.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class TransformStreamProducerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TransformStreamProducerApplication.class);
        application.run(args);
    }

}
