package com.workshop.kafka.init.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class TransformStreamConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TransformStreamConsumerApplication.class);
        application.run(args);
    }

}
