package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopStat;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StreamProducer  implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private final String topic_in = "window_topic";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);

        try {

            createTopics(topic_in);

            String key;

            for(String s: getList()) {

                key = getAlphaNumericString(3);
                String[] valueArray = s.split("::");
                WorkshopStat value = WorkshopStat.newBuilder()
                        .setIdent(valueArray[0])
                        .setReference(valueArray[1])
                        .setStat(Double.parseDouble(valueArray[2]))
                        .setTimestamp(valueArray[3])
                        .build();

                // Send record
                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(topic_in, key, value);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
    }

    private List<String> getList(){
        List<String> list = new ArrayList<>();
        list.add("id01::111111::8.2::2019-04-25T18:00:00-0700");
        list.add("id01::111111::4.5::2019-04-25T18:03:00-0700");
        list.add("id01::111111::5.1::2019-04-25T18:04:00-0700");
        list.add("id01::111111::2.0::2019-04-25T18:07:00-0700");
        list.add("id01::111111::8.3::2019-04-25T18:32:00-0700");
        list.add("id01::111111::3.4::2019-04-25T18:36:00-0700");
        list.add("id01::111111::4.2::2019-04-25T18:43:00-0700");
        list.add("id01::111111::7.6::2019-04-25T18:44:00-0700");
        list.add("id02::222222::4.9::2019-04-25T20:01:00-0700");
        list.add("id02::222222::5.6::2019-04-25T20:02:00-0700");
        list.add("id02::222222::9.0::2019-04-25T20:03:00-0700");
        list.add("id02::222222::6.5::2019-04-25T20:12:00-0700");
        list.add("id02::222222::2.1::2019-04-25T20:13:00-0700");
        list.add("id03::333333::3.6::2019-04-25T22:20:00-0700");
        list.add("id03::333333::6.0::2019-04-25T22:21:00-0700");
        list.add("id03::333333::7.0::2019-04-25T22:22:00-0700");
        list.add("id03::333333::4.6::2019-04-25T22:23:00-0700");
        list.add("id03::333333::7.1::2019-04-25T22:24:00-0700");
        list.add("id04::111111::9.9::2019-04-25T21:15:00-0700");
        list.add("id04::111111::8.6::2019-04-25T21:16:00-0700");
        list.add("id04::111111::4.2::2019-04-25T21:17:00-0700");
        list.add("id04::111111::7.0::2019-04-25T21:18:00-0700");
        list.add("id04::111111::9.5::2019-04-25T21:19:00-0700");
        list.add("id04::111111::3.2::2019-04-25T21:20:00-0700");
        list.add("id05::444444::3.5::2019-04-25T13:00:00-0700");
        list.add("id05::444444::4.0::2019-04-25T13:07:00-0700");
        list.add("id05::444444::5.1::2019-04-25T13:30:00-0700");
        list.add("id05::444444::2.0::2019-04-25T13:34:00-0700");
        return list;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private static void createTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                1,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }

}
