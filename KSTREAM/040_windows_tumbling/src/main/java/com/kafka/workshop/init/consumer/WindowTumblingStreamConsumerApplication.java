package com.kafka.workshop.init.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class WindowTumblingStreamConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WindowTumblingStreamConsumerApplication.class);
        application.run(args);
    }

}
