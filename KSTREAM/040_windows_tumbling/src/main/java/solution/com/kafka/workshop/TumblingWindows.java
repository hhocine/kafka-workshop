package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopStat;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

@Component
public class TumblingWindows implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StateStore.class);

    private String topic_in = "window_topic";
    private String topic_count= "window_count";


    private KafkaStreams streams = null;

    public KafkaStreams getStreams() {
        return streams;
    }

    public void setStreams(KafkaStreams streams) {
        this.streams = streams;
    }

    @Override
    public void run(ApplicationArguments args) {

        createTopics(topic_in);
        createTopics(topic_count);
        try {
            streams = new KafkaStreams(getTopology(), configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO set the DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG property
        streamsConfiguration.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WorkshopStatTimestampExtractor.class.getName());
        // TODO Play with the CACHE_MAX_BYTES_BUFFERING_CONFIG property
        // streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        try {
            streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG,
                    Files.createTempDirectory(new File("/tmp").toPath(), "tumbling-windows").toFile().getPath());
        }
        catch(IOException e) {
            // If we can't have our own temporary directory, we can leave it with the default. We create a custom
            // one because running the app outside of Docker multiple times in quick succession will find the
            // previous state still hanging around in /tmp somewhere, which is not the expected result.
        }
        return streamsConfiguration;
    }

    KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore("storeName");

    public Topology getTopology() {
        final StreamsBuilder builder = new StreamsBuilder();

         /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_in by specifying the Serdes for key and value with Consumed.with() method
                 Group the stream by key
                 Using the windowedBy method, set the duration to 10 minutes and the advance interval of 5 minutes
                 Transform to stream to a KTable<Windowed<String>, Long> unsing the count() method
                 Transform the KTable to KStream
                 Re-key the stream using the map method:
                                    set the key by extracting the key of the Windowed<String> object
                                    apply the toString() method to the count value
                 Produce the stream to window_count

        */
        KStream<String, String> windowStream =  builder.<String, WorkshopStat>stream(topic_in, Consumed.with(Serdes.String(), workshopStatAvroSerde()))
                .map((key, stat) -> new KeyValue<>(stat.getIdent().toString(), stat))
                .groupByKey()
                .windowedBy(TimeWindows.of(Duration.ofMinutes(10)).advanceBy(Duration.ofMinutes(5)))
                .count()
                .toStream()
                .map((Windowed<String> key, Long count) -> new KeyValue(key.key(), count.toString()));
                windowStream.to(topic_count, Produced.with(Serdes.String(), Serdes.String()));

        return builder.build();
    }

    /**
     * Get the SpecificAvroSerde for WorkshopStat
     * @return
     */
    private SpecificAvroSerde<WorkshopStat> workshopStatAvroSerde() {
        SpecificAvroSerde<WorkshopStat> workshopStatAvroSerde = new SpecificAvroSerde<>();
        workshopStatAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopStatAvroSerde;
    }

    /**
     *
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        serdeConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081"
        );
        return serdeConfig;
    }


    private static void createTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }
}

