package partial.com.kafka.workshop;

import com.kafka.workshop.avro.StatInvalid;
import com.kafka.workshop.avro.WorkshopStat;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.stereotype.Component;

@Component
public class ProcessorInvalidStat implements Processor<String, WorkshopStat> {

    private ProcessorContext context;

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public void process(String key, WorkshopStat value) {
        String header = new String(context.headers().lastHeader("reason").value());
        StatInvalid statInvalid = StatInvalid.newBuilder()
                .setId(value.getId())
                .setRef(value.getRef())
                .setStat(value.getStat())
                .setReason(header)
                .build();
        context.forward(key, statInvalid);

    }

    @Override
    public void close() {

    }

}
