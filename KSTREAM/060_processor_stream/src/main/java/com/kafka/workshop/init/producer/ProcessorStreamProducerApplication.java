package com.kafka.workshop.init.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class ProcessorStreamProducerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ProcessorStreamProducerApplication.class);
        application.run(args);
    }

}
