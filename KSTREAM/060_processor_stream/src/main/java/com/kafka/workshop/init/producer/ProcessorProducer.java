package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopStat;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ProcessorProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    private static String tire_general_topic = "tire-general-topic";
    private static String tire_valid_topic = "tire-valid-topic";
    private static String tire_invalid_topic = "tire-invalid-topic";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set the Schema Registry url
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the key and value serializers KafkaAvroSerializer
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        // Create an Producer instance with WorkshopKey key and WorkshopValue key
        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopStat workshopStat = null;

            for(int i=0; i<10; i++) {

                key = getAlphaNumericString(3);

                 workshopStat = WorkshopStat.newBuilder()
                        .setId(getAlphaNumericString(10))
                        .setStat(getNumeric())
                        .setRef(getAlphaNumericString(6))
                        .build();

                // TODO: Send record
                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(tire_general_topic, key, workshopStat);

                if(isValid()){
                    buildHeader(record, "validityDate", new Date().toString());
                    buildHeader(record, "type", "valid");
                } else {
                    buildHeader(record, "reason", getAlphaNumericString(30));
                    buildHeader(record, "type", "invalid");
                }
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }

        } finally {
            producer.close();
        }
    }

    private static void buildHeader(ProducerRecord<String, WorkshopStat> record, String headerKey, String headerValue) {
        RecordHeader recordHeader = new RecordHeader(headerKey, headerValue.getBytes());
        record.headers().add(recordHeader);
    }

    private static boolean isValid(){
        if(Math.random() < 0.7){
            return true;
        }
        return false;
    }
    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private static int getNumeric() {

        int index
                = (int) (1000 * Math.random());

        return index;
    }

    public static void createTopics() {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                tire_general_topic,
                3,
                (short) 1));
        topics.add(new NewTopic(
                tire_valid_topic,
                3,
                (short) 1));
        topics.add(new NewTopic(
                tire_invalid_topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }

}
