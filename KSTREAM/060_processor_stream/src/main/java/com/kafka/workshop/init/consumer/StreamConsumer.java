package com.kafka.workshop.init.consumer;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@Component
public class StreamConsumer implements ApplicationRunner {

    private static String tire_valid_topic = "tire-valid-topic";
    private static String tire_invalid_topic = "tire-invalid-topic";
    private static String tire_general_topic = "tire-general-topic";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupid1");
        // TODO: Set the Schema Registry url
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // TODO: Set the key and value serializers KafkaAvroSerializer
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);

        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        Consumer<String, GenericRecord> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(tire_general_topic, tire_invalid_topic, tire_valid_topic));

        try {
            while (true) {

                ConsumerRecords<String, GenericRecord> consumerRecords = consumer.poll(Duration.ofMillis(500));
                consumerRecords.forEach(record -> {
                    System.out.println("Consuming record with " + record.topic() + " === " + record.value().toString());
                });
                consumer.commitSync();
            }
        } finally {
            consumer.close();
        }
    }
}
