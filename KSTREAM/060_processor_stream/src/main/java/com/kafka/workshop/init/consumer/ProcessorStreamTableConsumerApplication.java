package com.kafka.workshop.init.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class ProcessorStreamTableConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ProcessorStreamTableConsumerApplication.class);
        application.run(args);
    }

}
