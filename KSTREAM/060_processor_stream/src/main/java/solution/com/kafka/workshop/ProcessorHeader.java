package solution.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopStat;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.To;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProcessorHeader implements Processor<String, WorkshopStat> {

    private ProcessorContext context;

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public void process(String key, WorkshopStat value) {
            /* TODO get the value of header "type" using the context object
                    if type is 'valid' return to ProcessorValidStat
                    if type is 'invalid' return to ProcessorInvalidStat
                    The retunr must be made using the forward method of the context object
             */
        String headerKey = new String(context.headers().lastHeader("type").value());
        String processor;
        switch (headerKey) {
            case "valid":
                processor = "ProcessorValidStat";
                break;
            case "invalid":
                processor = "ProcessorInvalidStat";
                break;
            default:
                processor = null;
                break;
        }
        // TODO forward the context
        context.forward(key, value, To.child(processor));
    }

    @Override
    public void close() {

    }

}
