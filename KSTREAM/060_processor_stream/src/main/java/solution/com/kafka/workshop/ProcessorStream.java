package solution.com.kafka.workshop;


import com.kafka.workshop.avro.StatInvalid;
import com.kafka.workshop.avro.StatValid;
import com.kafka.workshop.avro.WorkshopStat;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ProcessorStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(ProcessorStream.class);

    private String processor_topic = "processor-topic";
    private String processor_valid_topic = "processor-valid-topic";
    private String processor_invalid_topic = "processor-invalid-topic";


    @Override
    public void run(ApplicationArguments args) {

        createTopics(processor_topic);
        createTopics(processor_invalid_topic);
        createTopics(processor_valid_topic);
        try {
            KafkaStreams streams = new KafkaStreams(getTopology(), configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "processor-app-id");
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG,"processor-cli-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }

    private Topology getTopology() {
        // TODO Create a Topology instance
        Topology topology = new Topology();
        /* TODO
                  Build a topology using source, processor and sink in order to :
                  consume the processor-topic topic using addSource() method
                  apply the ProcessorHeader to each record using addProcessor method()
                  apply ProcessorValidStat if value is valid using addProcessor method()
                  apply ProcessorInvalidStat if value is invalid using addProcessor method()
                  produce to processor-valid-topic topic of stat is valid using a addSink() method
        */
        topology.addSource("Source", new StringDeserializer(), getDeserializer(), processor_topic)
                //define source topic
                .addProcessor("ProcessorHeader", ProcessorHeader::new, "Source")
                .addProcessor("ProcessorValidStat", ProcessorValidStat::new, "ProcessorHeader")
                .addProcessor("ProcessorInvalidStat", ProcessorInvalidStat::new, "ProcessorHeader")
                .addSink("SinkValidStat", processor_valid_topic, new StringSerializer(), getTireValidSerializer(), "ProcessorValidStat")
                .addSink("SinkInvalidStat", processor_invalid_topic, new StringSerializer(), getTireInvalidSerializer(), "ProcessorInvalidStat");
        return topology;
    }


    private SpecificAvroSerializer getTireValidSerializer() { ;
        SpecificAvroSerializer<StatValid> serializer = new SpecificAvroSerializer<>();
        serializer.configure(getSchemaRegistryAvroConf(), false);
        return serializer;
    }

    private SpecificAvroSerializer getTireInvalidSerializer() {
        SpecificAvroSerializer<StatInvalid> serializer = new SpecificAvroSerializer<>();
        serializer.configure(getSchemaRegistryAvroConf(), false);
        return serializer;
    }

    private SpecificAvroDeserializer getDeserializer() {
        SpecificAvroDeserializer<WorkshopStat> deserializer = new SpecificAvroDeserializer<>();
        deserializer.configure(getSchemaRegistryAvroConf(), false);
        return deserializer;
    }

    /**
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        serdeConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081"
        );
        return serdeConfig;
    }

    public static void createTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }
}

