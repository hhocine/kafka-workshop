#### This exercise aims at writing a KSTREAM java application using the processor API  

The objective of this exercise is to:
- read records from processor-topic topic 
- depending on the header send the record to a processor-valid-topic topic or a processor-invalid-topic topic    
All of these using the Processor API    

    
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Configure the KSTREAM topology using the Processor API  
Follow the TODO instructions in the solution.com.kafka.workshop.ProcessorStream and solution.com.kafka.workshop.ProcessorHeader java classes   

####Start the Stream application
Start the application  


#### Consume example records
Use the com.workshop.kafka.init.consumer.ProcessorStreamTableConsumerApplication class to consumer records from processor-valid-topic and processor-invalid-topic topics     
Check that filtering is made correctly    

#### Produce example records
Use the com.kafka.workshop.init.producer.SplitStreamProducerApplication class to produce records to processor-topic topic 
