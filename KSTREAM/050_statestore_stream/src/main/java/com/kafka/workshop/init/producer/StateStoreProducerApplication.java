package com.kafka.workshop.init.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class StateStoreProducerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(StateStoreProducerApplication.class);
        application.run(args);
    }

}
