package partial.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.state.HostInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import partial.com.kafka.workshop.controller.MetadataService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class StateStore implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StateStore.class);

    private String topic_in = "statestore-topic-in";

    private KafkaStreams streams;

    private MetadataService metadataService;

    private HostInfo hostInfo;

    @Autowired
    Environment environment;

    @Override
    public void run(ApplicationArguments args) {

        try {
            // Get Topology
            // Step 1 and Step 2 get KTable topology
            streams = new KafkaStreams(getTopology(), configureStream());
            // Step 3 get GlobalKTable topology
            //    streams = new KafkaStreams(getGlobalTopology(), configureStream());
            streams.start();

            // Step 2 Create host info instance
            // hostInfo = new HostInfo("localhost", Integer.parseInt(environment.getProperty("local.server.port")));
            // Step 2 Create instance of metadaService
            //  metadataService = new MetadataService(this.streams);
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    private Properties configureStream() throws IOException {
        Properties streamsConfiguration = new Properties();
        // Set the bootstrap servers using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "statestore-app-id");
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, "statestore-client-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // TODO create temp directory
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, Files.createTempDirectory(new File("/tmp").toPath(), "stateStore_stream" + "_" + environment.getProperty("local.server.port")).toFile().getPath());


        /*
           TODO Step 2
           Register the StreamsConfig.APPLICATION_SERVER_CONFIG with your localhost:environment.getProperty("local.server.port")
         */
         /* TODO Step 2
           Set the NUM_STANDBY_REPLICAS_CONFIG to 2
        */

        return streamsConfiguration;
    }

    /**
     * Step 1 and 2
     * build the stream topology to build a KTable
     *
     * @return
     */
    private Topology getTopology() {
        // TODO Create StreamsBuilder instance
        final StreamsBuilder builder = null;
         /*
            TODO Create a KTABLE instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
                 Materialized the KTable into a local state store, specify the Serdes for Key and value
                 Name the store "simpleStore"
        */
        KTable<String, WorkshopValue> workshopValueKTable = null;

        return builder.build();
    }

    /**
     * Step 3
     * build the stream topology to build a GlobalKTable
     *
     * @return
     */
    private Topology getGlobalTopology() {
        // TODO Create StreamsBuilder instance
        final StreamsBuilder builder = null;
        /*
            TODO Create a GlobalKTABLE instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
                 Materialized the KTable into a local state store, specify the Serdes for Key and value
                 Name the store "globalStore"
        */
        GlobalKTable<String, WorkshopValue> workshopValueGlobalKTable = null;
        return builder.build();
    }

    private SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde() {
        SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde = new SpecificAvroSerde<>();
        workshopValueAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopValueAvroSerde;
    }

    /**
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        serdeConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081"
        );
        return serdeConfig;
    }

    public KafkaStreams getStreams() {
        return streams;
    }

    public MetadataService getMetadataService() {
        return metadataService;
    }

    public HostInfo getHostInfo() {
        return hostInfo;
    }


}

