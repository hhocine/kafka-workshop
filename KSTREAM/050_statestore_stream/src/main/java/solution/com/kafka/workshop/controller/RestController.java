package solution.com.kafka.workshop.controller;

import com.kafka.workshop.avro.WorkshopValue;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import solution.com.kafka.workshop.StateStore;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private StateStore service;

    @Autowired
    RestTemplate restTemplate;

    /**
     * Step1
     * Get key in case of local state store that is not shared
     * @return
     */
    @RequestMapping(value = "/stateStore/allByInstance/key", method = RequestMethod.GET)
    public ResponseEntity<String> getKeyByInstance(@RequestParam("key") String key){
        WorkshopValue value = getValue("simpleStore", key);
        return ResponseEntity.ok().body(value.toString());
    }


    /**
     * Step1
     * Get all values in case of local state store that is not shared
     * @return
     */
    @RequestMapping(value = "/stateStore/allByInstance", method = RequestMethod.GET)
    public ResponseEntity<String> getAllByInstance() {
        String movies = getValues("simpleStore");
        return ResponseEntity.ok().body(movies);
    }


    /**
     * Step2
     * Get key in case of shared local state store
     * @param key
     * @return
     */
    @RequestMapping(value = "/stateStore/key", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<String> getByKey(@RequestParam("key") String key) {

        final HostStoreInfo hostStoreInfo;
        try {
            hostStoreInfo = streamsMetadataForStoreAndKey("simpleStore", key);
            if (!thisHost(hostStoreInfo)){
                return fetchByKey(hostStoreInfo, "stateStore/key?key="+key);
            }
            WorkshopValue value = getValue("simpleStore", key);
            return ResponseEntity.ok().body(value.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("KO");
        }
    }

    /**
     * Step2
     * Get all values in case of shared local state store
     * @return
     */
    @RequestMapping(value = "/stateStore/all", method = RequestMethod.GET)
    public ResponseEntity<String> getAll() {
        final List<HostStoreInfo> hostStoreInfoList = streamsMetadataForStore("simpleStore");
        String workshopValues = "";
        for(HostStoreInfo hostStoreInfo: hostStoreInfoList) {
            if (!thisHost(hostStoreInfo)) {
                workshopValues += fetchAll(hostStoreInfo, "stateStore/allByInstance").getBody();
             } else {
                workshopValues += getValues("simpleStore");
            }
        }
        return ResponseEntity.ok().body(workshopValues);
    }

    /**
     * Step3
     * Get key in case of global local state store
     * @param key
     * @return
     */
    @RequestMapping(value = "/stateStore/global/key", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<String> getGlobalByKey(@RequestParam("key") String key) {
        try {

            KafkaStreams stream = service.getStreams();

            ReadOnlyKeyValueStore<String, WorkshopValue> keyValueStore =
                    stream.store("globalStore", QueryableStoreTypes.keyValueStore());

            return ResponseEntity.ok().body(keyValueStore.get(key).toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("KO");
        }
    }

    /**
     * Step3
     * Get all values in case og global state store
     * @return
     */
    @RequestMapping(value = "/stateStore/global/all", method = RequestMethod.GET)
    public ResponseEntity<String> getGlobalAll() {
        String values = getValues("globalStore");
        return ResponseEntity.ok().body(values);
    }

   /**
     *
     * Return all values from a state store
     * @return
     */
    private String getValues(String stateStore){
        String values = "";
        // TODO get a reference to the statore
        ReadOnlyKeyValueStore<String, WorkshopValue> keyValueStore = service.getStreams().store(stateStore, QueryableStoreTypes.keyValueStore());
        // TODO get all values in the state store
        KeyValueIterator<String, WorkshopValue> it = keyValueStore.all();
        // TODO iterate through the KeyValueIterator, build the 'values' string
        while (it.hasNext()) {
            KeyValue<String, WorkshopValue> keyValue = it.next();
            values += keyValue.value + "<br>";
        }
        return values;
    }

    /**
     *
     * Return the value in the state store from key
     * @return
     */
    private WorkshopValue getValue(String stateStore, String key){
        // TODO get a reference to the statore
        ReadOnlyKeyValueStore<String, WorkshopValue> keyValueStore = service.getStreams().store(stateStore, QueryableStoreTypes.keyValueStore());
        // TODO get the value with the supplied key
        WorkshopValue value = keyValueStore.get(key);
        return value;
    }

    /**
     * Find the metadata for the instance of this Kafka Streams Application that has the given
     * store and would have the given key if it exists.
     * @param store   Store to find
     * @param key     The key to find
     * @return {@link HostStoreInfo}
     */
    public HostStoreInfo streamsMetadataForStoreAndKey(final String store,
                                                       final String key) throws Exception {
        return this.service.getMetadataService().streamsMetadataForStoreAndKey(store, key, new StringSerializer());
    }

    /**
     *
     * @param store
     * @return
     */
    public List<HostStoreInfo> streamsMetadataForStore(final String store) {
        return service.getMetadataService().streamsMetadataForStore(store);
    }

    /**
     *
     * @param host
     * @param path
     * @return
     */
    private ResponseEntity<String> fetchByKey(final HostStoreInfo host, final String path) {
        return restTemplate.getForEntity(String.format("http://%s:%d/%s", host.getHost(), host.getPort(), path), String.class);
    }

    /**
     *
     * @param host
     * @param path
     * @return
     */
    private ResponseEntity<String> fetchAll(final HostStoreInfo host, final String path) {
        return restTemplate.getForEntity(String.format("http://%s:%d/%s", host.getHost(), host.getPort(), path), String.class);
    }

    /**
     *
     * @param host
     * @return
     */
    private boolean thisHost(final HostStoreInfo host) {
        return host.getHost().equals(service.getHostInfo().host()) &&
                host.getPort() == service.getHostInfo().port();
    }





}
