package solution.com.kafka.workshop.controller;

import com.kafka.workshop.avro.WorkshopValue;

import java.util.ArrayList;
import java.util.List;

public class WorkshopValueList {

    private List<WorkshopValue> workshopValueList;

    public List<WorkshopValue> getWorkshopValueList() {
        return workshopValueList;
    }
}
