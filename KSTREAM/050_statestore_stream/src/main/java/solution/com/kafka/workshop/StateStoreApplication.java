package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class StateStoreApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(StateStoreApplication.class);
        application.run(args);
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
	
}
