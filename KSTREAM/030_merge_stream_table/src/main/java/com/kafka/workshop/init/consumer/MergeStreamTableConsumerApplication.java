package com.kafka.workshop.init.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class MergeStreamTableConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MergeStreamTableConsumerApplication.class);
        application.run(args);
    }

}
