package partial.com.kafka.workshop;


import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import com.kafka.workshop.avro.WorkshopStatEnrichedFiltered;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MergeStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(MergeStream.class);

    private final String topic_stream_in = "merge-stat-topic";
    private final String topic_table_in = "merge-ref-topic";
    private final String topic_intermediate = "merge-intermediate-topic";
    private final String topic_out = "merge-out-topic";

    @Override
    public void run(ApplicationArguments args) {

        createTopics(topic_table_in);
        createTopics(topic_stream_in);
        createTopics(topic_intermediate);
        createTopics(topic_out);

        try {
            KafkaStreams streams = new KafkaStreams(getTopology(), configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     * @return
     */
    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "merge-app-id");
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG,"merge-cli-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    private Topology getTopology(){
        final StreamsBuilder builder = new StreamsBuilder();
        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_in by specifying the Serdes for key and value with Consumed.with() method
                 Re-key the stream using the map method:
                                    set the key with reference of the WorkshopStat object
                                    keep the same value
        */
        KStream<String, WorkshopStat> statStream = null;
        /*
            TODO Create a KTable instance from the StreamBuilder instance
                 Consume topic_table_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KTable<String, WorkshopRef> refTable = null;
        /*
            TODO Configure the CustomJoiner class and create an instance
        */
        CustomJoiner customJoiner = new CustomJoiner();
        /*
            TODO Join of statStream and refTable
                 Use the join() method on the statStream instance,
                 pass the three following arguments: refTable, customJoiner and Joined.valueSerde
        */
        KStream<String, WorkshopStatEnriched> enrichedStream = null;
        /*
            TODO In one stage:
                    Produce the new stream to topic_intermediate
                    Change the value with a WorkshopStatEnrichedFiltered object
                            set statOne property with  Integer.parseInt(workshopStatEnriched.getStat().split("::")[0])
                            set statTwo property wtith Integer.parseInt(workshopStatEnriched.getStat().split("::")[1])
                    Filter the stream, retains only WorkshopStatEnrichedFiltered with statOne property higher than 100
                    Produce the stream to merge-out-topic
        */
        KStream<String, WorkshopStatEnrichedFiltered> filterStream = null;
        return builder.build();

    }

    /**
     * Get the SpecificAvroSerde for WorkshopRef
     * @return
     */
    private SpecificAvroSerde<WorkshopRef> workshopRefAvroSerde() {
        SpecificAvroSerde<WorkshopRef> workshopRefAvroSerde = new SpecificAvroSerde<>();
        workshopRefAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopRefAvroSerde;
    }

    /**
     * Get the SpecificAvroSerde for WorkshopStat
     * @return
     */
    private SpecificAvroSerde<WorkshopStat> workshopStatAvroSerde() {
        SpecificAvroSerde<WorkshopStat> workshopStatAvroSerde = new SpecificAvroSerde<>();
        workshopStatAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopStatAvroSerde;
    }
    /**
     * Get the SpecificAvroSerde for WorkshopStatEnriched
     * @return
     */
    private SpecificAvroSerde<WorkshopStatEnriched> workshopStatEnrichedAvroSerde() {
        SpecificAvroSerde<WorkshopStatEnriched> workshopStatEnrichedAvroSerde = new SpecificAvroSerde<>();
        workshopStatEnrichedAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopStatEnrichedAvroSerde;
    }
    /**
     * Get the SpecificAvroSerde for WorkshopStatEnrichedFiltered
     * @return
     */
    private SpecificAvroSerde<WorkshopStatEnrichedFiltered> workshopStatEnrichedFilteredAvroSerde() {
        SpecificAvroSerde<WorkshopStatEnrichedFiltered> workshopStatEnrichedFilteredAvroSerde = new SpecificAvroSerde<>();
        workshopStatEnrichedFilteredAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopStatEnrichedFilteredAvroSerde;
    }

    /**
     *
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        serdeConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081"
        );
        return serdeConfig;
    }

    private static void createTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }
}

