package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class MergeStreamApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MergeStreamApplication.class);
        application.run(args);
    }
	
}
