package com.kafka.workshop.init.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class SplitStreamProducerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SplitStreamProducerApplication.class);
        application.run(args);
    }

}
