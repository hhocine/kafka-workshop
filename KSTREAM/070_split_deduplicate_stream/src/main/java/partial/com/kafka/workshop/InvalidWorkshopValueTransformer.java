package partial.com.kafka.workshop;

import com.kafka.workshop.avro.InvalidWorkshopValue;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;

/*
    TODO Implements the Transformer<K, V, R> interface
 */
public class InvalidWorkshopValueTransformer {

    private ProcessorContext context;

    /*
        TODO Override the init method and set the context
     */


    /*
        TODO Override the transform method
             Access the headers and get the value of the "reason" key, set it to reason property of the InvalidWorkshopValue
     */



    /*
        TODO Override the close method, do nothing
     */

}
