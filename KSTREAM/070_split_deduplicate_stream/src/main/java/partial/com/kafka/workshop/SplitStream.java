package partial.com.kafka.workshop;

import com.kafka.workshop.avro.InvalidWorkshopValue;
import com.kafka.workshop.avro.ValidWorkshopValue;
import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SplitStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(SplitStream.class);

    private final String topic_in = "split-topic-in";
    private final String topic_valid_out = "split-valid-topic-out";
    private final String topic_invalid_out = "split-invalid-topic-out";

    @Override
    public void run(ApplicationArguments args) {

        createTopics(topic_in);
        createTopics(topic_valid_out);
        createTopics(topic_invalid_out);

        try {
            KafkaStreams streams = new KafkaStreams(getTopology(), configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     * @return
     */
    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "merge-app-id");
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG,"merge-cli-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    private Topology getTopology() {

        final StreamsBuilder builder = new StreamsBuilder();

        /*
            TODO Create storeBuilder to manage record deduplication
                 Use the  Stores.keyValueStoreBuilder() static method
                 Set an inMemoryKeyValueStore
         */
        final StoreBuilder<KeyValueStore<String, ValidWorkshopValue>> deduplicatorStoreBuilder = null;

        // TODO Add the state store to builder


        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, WorkshopValue> stream = null;
        /*
            TODO Branch (or split) the stream instance into two streams depending of the valid property of WorkshopValue object
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, WorkshopValue>[] branches = null;

        /*
            TODO Set the value with a InvalidWorkshopValue object
                 Call the API Processor using the transform(() -> new InvalidWorkshopValueTransformer()) method
                 Produce the new stream to topic_invalid_out by specifying the Serdes for key and value with Produces.with() method
         */


         /*
            TODO Set the value with a ValidWorkshopValue object
                 Call the API Processor using the transform(() -> new ValidWorkshopValueTransformer()) method
                 Produce the new stream to topic_valid_out by specifying the Serdes for key and value with Produces.with() method
         */


        return builder.build();
    }

    /**
     * Get the SpecificAvroSerde for WorkshopValue
     * @return
     */
    private SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde() {
        SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde = new SpecificAvroSerde<>();
        workshopValueAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return workshopValueAvroSerde;
    }
    /**
     * Get the SpecificAvroSerde for ValidWorkshopValue
     * @return
     */
    private SpecificAvroSerde<ValidWorkshopValue> validWorkshopValueAvroSerde() {
        SpecificAvroSerde<ValidWorkshopValue> validWorkshopValueAvroSerde = new SpecificAvroSerde<>();
        validWorkshopValueAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return validWorkshopValueAvroSerde;
    }
    /**
     * Get the SpecificAvroSerde for InvalidWorkshopValue
     * @return
     */
    private SpecificAvroSerde<InvalidWorkshopValue> invalidWorkshopValueAvroSerde() {
        SpecificAvroSerde<InvalidWorkshopValue> invalidWorkshopValueAvroSerde = new SpecificAvroSerde<>();
        invalidWorkshopValueAvroSerde.configure(getSchemaRegistryAvroConf(), false);
        return invalidWorkshopValueAvroSerde;
    }

    /**
     *
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        serdeConfig.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081"
        );
        return serdeConfig;
    }

    private static void createTopics(String topic) {
        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }

}

