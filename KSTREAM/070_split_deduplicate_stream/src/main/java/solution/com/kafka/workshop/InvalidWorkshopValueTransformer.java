package solution.com.kafka.workshop;

import com.kafka.workshop.avro.InvalidWorkshopValue;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;

/*
    TODO Implements the Transformer<K, V, R> interface
 */
public class InvalidWorkshopValueTransformer implements Transformer<String, InvalidWorkshopValue, KeyValue<String, InvalidWorkshopValue>> {

    private ProcessorContext context;

    /*
        TODO Override the init method and set the context
     */
    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    /*
        TODO Override the transform method
             Access the headers and get the value of the "reason" key, set it to reason property of the InvalidWorkshopValue
     */
    @Override
    public KeyValue<String, InvalidWorkshopValue> transform(String key, InvalidWorkshopValue value) {
        Headers headers = context.headers();
        value.setReason(new String(headers.lastHeader("reason").value()));
        return new KeyValue<>(key, value);
    }


    /*
        TODO Override the close method, do nothing
     */
    @Override
    public void close() {}
}
