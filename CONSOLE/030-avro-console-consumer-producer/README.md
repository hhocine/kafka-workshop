#### This exercise aims at producing and consuming AVRO records using the Kafka avro console producer and Kafka avro console consumer

#### Start the Kafka cluster
In the CONSOLE directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry 
The docker-compose file contains also a base container that allows to run Kafka commands

####Interact with Kafka cluster
Get into the base container    
```docker exec -it base bash```
		
#### Create a topic
Create a topic named consoleAvroTopic using the kafka-topics command, replicated 3 times and with 3 partitions  
```kafka-topics --create --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic consoleAvroTopic --replication-factor 3 --partitions 3```

#### List existing topics
List the existing topics using the --list option of the kafka-topics command  
```kafka-topics --list --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092```  

#### Produce avro records
Using the Kafka avro console producer, send records to the topic consoleAvroTopic  
Specify the schema-registry urls with the --property option  
```kafka-avro-console-producer --broker-list kafka1:19092,kafka2:29092,kafka3:39092 --topic consoleAvroTopic --property schema.registry.url=http://schema-registry:8081 --property value.schema='{"type":"record","name":"consoleRecord","fields":[{"name":"prop1","type":"string"}, {"name":"prop2","type":"string", "default": "prop2 default value"} ]}'```  
Send some records  
```{"prop1": "my prop1 first value", "prop2": "my prop2 first value"}```  
```{"prop1": "my prop1 second value", "prop2": "my prop2 second value"}```  
```{"prop1": "my prop1 third value", "prop2": "my prop2 third value"}```  

#### Produce avro records with a new schema
Make the schema evolves, delete the prop2 property, add the prop3 property  
Specify the schema-registry urls with the --property option  
```kafka-avro-console-producer --broker-list kafka1:19092,kafka2:29092,kafka3:39092 --topic consoleAvroTopic --property schema.registry.url=http://schema-registry:8081 --property value.schema='{"type":"record","name":"consoleRecord","fields":[{"name":"prop1","type":"string"}, {"name":"prop3","type":"string", "default": "my prop 3 default"} ]}'```  
Send some records  
```{"prop1": "my new prop1 first value", "prop3": "my prop3 first value"}```
```{"prop1": "my new prop1 second value", "prop3": "my prop3 second value"}```  
```{"prop1": "my new prop1 third value", "prop3": "my prop3 third value"}```  

#### Check Schema value
Using a API testing toll (Postman), list the existing schemas  
```http://localhost:8081/subjects```  
Get the versions of the consoleAvroTopic value schema      
```http://localhost:8081/subjects/consoleAvroTopic-value/versions```  
Display the last version  
```http://localhost:8081/subjects/consoleAvroTopic-value/versions/{version_number}```  
In a more readable way  
```http://localhost:8081/subjects/consoleAvroTopic-value/versions/{version_number}/schema```  

#### Consume avro records
Open another console and get into the base container    
```docker exec -it base bash```  

Using the Kafka console consumer, get records from the topic consoleAvroTopic with a consumer group group1  
```kafka-avro-console-consumer --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic consoleAvroTopic --property schema.registry.url=http://schema-registry:8081 --from-beginning```  
All records are consumes using the schema in their header  

#### Stop and remove containers 
 ```docker-compose stop```   
 ```docker-compose rm -f```
