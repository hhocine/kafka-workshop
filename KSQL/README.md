#### Those exercises aim at using KSQL

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command
```docker-compose up -d```  
The cluster is composed of 1 zookeeper and 1 broker  
The docker-compose file contains also a base container that allows to run Kafka commands
   