#### This exercise aims at transforming a stream of records with KSQL

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker, 1 schema-registry, 1 KSQL server and 1 KSQL client  
The docker-compose file contains also a base container that allows to run Kafka commands

####Interact with the KSQL Client
Get into the ksql client container    
``docker exec -it ksql-cli ksql http://ksql-server:8088``

####Create a STREAM
Create a stream that represents stats from a stats topic  
Name the topic workshop_stats_topic, it must have 3 partitions  
Name the stream workshop_stats with 3 columns: id (int), stats (varchar), type (varchar)    
Set the key with id  
Set the format with AVRO  
``CREATE STREAM workshop_stats...``  
Show details of the created stream  
``DESCRIBE workshop_stats;``  

####Insert values into the stream
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '110::120 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::80 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (444444, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::110 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (666666, '60::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '90::90 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::160 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '10::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '150::100 bar', '4season');``  

####Set the auto offset resset to earliest
In order to read events from beginning, set the auto.offset.reset to earliest  
``SET 'auto.offset.reset' = 'earliest';``

####Select  the workshop_stats stream and transform it 
Split the stats  columns into a temperature column (int) and a pressure column (varchar)  
``SELECT id, ...``  
Press CTRL+C to interrupt  

####Create a new stream workshop_stats_transform from the select above
Split the stats columns into a temperature column (int) and a pressure column (varchar)  
Put the contents into a new topic named  parsed_workshop_stats_topic  
``CREATE STREAM  workshop_stats_transform...`` 

####Print out the existing topics
Check that the parsed_workshop_stats_topic has been created  
``SHOW topics``  

####Print out the contents of the output streams
Print out the contents of the parsed_workshop_stats_topic  
``PRINT ...`` 

####Select the workshop_stats_transform stream
Select all records from  workshop_stats_transform stream
``SELECT * ...``  
Press CTRL+C to interrupt 