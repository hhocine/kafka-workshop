#### This exercise aims at count elements from a stream 

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

####Interact with the KSQL Client
Get into the ksql client container    
``docker exec -it ksql-cli ksql http://ksql-server:8088``

####Create a STREAM
Create a stream that represents stats from a stats topic  
Name the topic workshop_stats_count_topic, it must have 3 partitions  
Name the stream workshop_stats_count with 4 columns: id (int), stats (varchar), type (varchar), timestam (varchar)  
Set the key with id  
Set the format with AVRO  
``CREATE STREAM workshop_stats_count...``  
Show details of the created stream  
``DESCRIBE workshop_stats_count;``  

####``INSERT values into the stream
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 12, '4season', '2019-07-09 01:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 10, 'summer', '2019-07-09 05:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 90, '4season', '2019-07-09 07:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 80, 'winter', '2019-07-09 09:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 50, 'summer', '2019-07-09 08:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 60, 'winter', '2019-07-09 12:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 90, '4season', '2019-07-09 15:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 80, 'winter', '2019-07-09 22:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 150, 'summer', '2019-07-09 05:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 120, '4season', '2019-07-09 02:00:00');``    
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (5, 10, 'summer', '2019-07-09 18:00:00');``    

####Count number of records
Count the number of records received by id using the COUNT function  
``SELECT id, COUNT(stats)...``  
Count the total number stats by type using the SUM function   
``SELECT id, SUM(stats)...``  