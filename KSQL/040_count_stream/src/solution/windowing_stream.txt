CREATE STREAM workshop_stats_count (id int, stats int, type varchar, timestamp varchar)
    WITH (kafka_topic='workshop_stats_count_topic',
          key='id',
          partitions=3,
          value_format='avro');

DESCRIBE workshop_stats_count;

INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 12, '4season', '2019-07-09 01:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 10, 'summer', '2019-07-09 05:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 90, '4season', '2019-07-09 07:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 80, 'winter', '2019-07-09 09:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 50, 'summer', '2019-07-09 08:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 60, 'winter', '2019-07-09 12:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 90, '4season', '2019-07-09 15:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 80, 'winter', '2019-07-09 22:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 150, 'summer', '2019-07-09 05:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 120, '4season', '2019-07-09 02:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (5, 10, 'summer', '2019-07-09 18:00:00');

SET 'auto.offset.reset' = 'earliest';

SELECT id, COUNT(stats) AS COUNT FROM workshop_stats_window GROUP BY id;

SELECT type, SUM(stats) AS SUM FROM workshop_stats_window GROUP BY type;


