#### #### This exercise aims at re-keying a stream of records with KSQL

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

####Interact with the KSQL Client
Get into the ksql client container    
``docker exec -it ksql-cli ksql http://ksql-server:8088``

####Create a STREAM
Create a Kafka topic and stream to represent the publications  
Create a stream that represents info from an info topic  
Name the topic workshop_info_topic, it must have 3 partitions  
Name the stream workshop_info with 2 columns: id (int), description (varchar)  
Do not set the key  
Set the format with AVRO  
``CREATE STREAM workshop_info...``  
Show details of the created stream  
``DESCRIBE workshop_info;``  

####Insert values into the stream
``INSERT INTO workshop_info (id, description) VALUES (111111, 'description of 1');``  
``INSERT INTO workshop_info (id, description) VALUES (222222, 'description of 2');``  
``INSERT INTO workshop_info (id, description) VALUES (333333, 'description of 3');``  
``INSERT INTO workshop_info (id, description) VALUES (444444, 'description of 4');``  
``INSERT INTO workshop_info (id, description) VALUES (555554, 'description of 5');``  
``INSERT INTO workshop_info (id, description) VALUES (666666, 'description of 6');``  

####Set the auto offset resset to earliest  
In order to read events from beginning, set the auto.offset.reset to earliest  
``SET 'auto.offset.reset' = 'earliest';``

####Select  the workshop_info stream
Examine the workshop_info stream  
``SELECT ROWKEY, id, ...``  
Note the key is null  
Press CTRL+C to interrupt  

####Re-key the stream
Using the PARTITION BY clause, create a new stream named workshop_info_rekeyed with the id as key  
``CREATE STREAM  workshop_info_rekeyed...`` 

####Select the workshop_info_rekeyed stream
Check the workshop_info_rekeyed content  

####Identify the underlying topic  
List the existing topics  
``SHOW topics``  

####Print out the contents of the underlying topic 
Print out the contents of the WORKSHOP_INFO_REKEYED topic  
``PRINT ...`` 